import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameScraperResultsComponent } from './game-scraper-results/game-scraper-results.component';
import { GameScraperResultComponent } from './game-scraper-result/game-scraper-result.component';
import { GameScraperDetailsComponent } from './game-scraper-details/game-scraper-details.component';
import { GameEditorComponent } from './game-editor/game-editor.component';
import { GamesLibraryComponent } from './games-library/games-library.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { GameScraperComponent } from './game-scraper/game-scraper.component';
import { ImageSelectorComponent } from './image-selector/image-selector.component';
import { ArtImageComponent } from './art-image/art-image.component';
import { GamesLibraryItemComponent } from './games-library-item/games-library-item.component';
import { MenuSideComponent } from './menu-side/menu-side.component';
import { MenuTopComponent } from './menu-top/menu-top.component';
import { TempoDeleteMeComponent } from './tempo-delete-me/tempo-delete-me.component';

@NgModule({
  declarations: [
    AppComponent,
    GameScraperResultsComponent,
    GameScraperResultComponent,
    GameScraperDetailsComponent,
    GameEditorComponent,
    GamesLibraryComponent,
    MainMenuComponent,
    GameScraperComponent,
    ImageSelectorComponent,
    ArtImageComponent,
    GamesLibraryItemComponent,
    MenuSideComponent,
    MenuTopComponent,
    TempoDeleteMeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
