import { Component, OnInit, ElementRef, Self, HostListener } from '@angular/core';
import { GameManagerService } from '../services/game-manager.service';
import { GameManagerGame } from '../objects/game-manager-game';

@Component({
  selector: 'app-games-library',
  templateUrl: './games-library.component.html',
  styleUrls: ['./games-library.component.scss']
})
export class GamesLibraryComponent implements OnInit {

  games: GameManagerGame[];
  columns = 3;

  constructor(@Self() private el: ElementRef, private manager: GameManagerService) { }

  ngOnInit(): void {
    this.games = [];
    this.manager.getGames().subscribe(
      page => {
        for (const game of page.content) {
          this.games.push(game);
        }
        console.log(this.games);
      },
      error => {
        console.log(error);
      },
      () => {
        console.log('end');
      }
    );
    this.onResize('test');
  }
  @HostListener('window: resize', ['$event']) 
  onResize(event: any) {
    this.columns = Math.floor(this.el.nativeElement.offsetWidth / 120);
  }

  getGridStyle() {
    const width = this.columns * 120;
    return {
      display: 'grid',
      'grid-template-columns' : `repeat(${this.columns}, 1fr)`,
      'width.px' : width,
      'margin-left' : 'auto',
      'margin-right' : 'auto',
      'justify-items' : 'center',
      'place-self' : 'center',
    };
  }

  getIconStyle() {
    return {
      'background-color' : '#' + Math.floor(Math.random() * 16777215).toString(16),
      width : '118px',
      height : '178px',
      border : 'solid black 1px'
    };
  }
}
