import { Component, OnInit, Input } from '@angular/core';
import { GiantbombGame } from '../objects/giantbomb-game';

@Component({
  selector: 'app-game-scraper-result',
  templateUrl: './game-scraper-result.component.html',
  styleUrls: ['./game-scraper-result.component.scss']
})
export class GameScraperResultComponent implements OnInit {
  @Input() game: GiantbombGame;

  constructor() { }

  ngOnInit(): void {
  }

}
