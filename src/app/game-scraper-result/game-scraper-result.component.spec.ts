import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameScraperResultComponent } from './game-scraper-result.component';

describe('GameScraperResultComponent', () => {
  let component: GameScraperResultComponent;
  let fixture: ComponentFixture<GameScraperResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameScraperResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameScraperResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
