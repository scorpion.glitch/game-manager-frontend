import { Component, OnInit, Input } from '@angular/core';
import { GameManagerGame } from '../objects/game-manager-game';
import { GameManagerService } from '../services/game-manager.service';

@Component({
  selector: 'app-game-editor',
  templateUrl: './game-editor.component.html',
  styleUrls: ['./game-editor.component.scss']
})
export class GameEditorComponent implements OnInit {
  @Input() gameManagerGame: GameManagerGame;

  constructor(private gameManager: GameManagerService) { }

  ngOnInit(): void {}
}
