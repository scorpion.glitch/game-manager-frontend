import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TempoDeleteMeComponent } from './tempo-delete-me.component';

describe('TempoDeleteMeComponent', () => {
  let component: TempoDeleteMeComponent;
  let fixture: ComponentFixture<TempoDeleteMeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TempoDeleteMeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TempoDeleteMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
