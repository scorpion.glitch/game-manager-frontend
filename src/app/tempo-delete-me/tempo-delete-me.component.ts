import { Component, OnInit } from '@angular/core';
import { GameClientService } from '../services/game-client.service';
import { DiscCarousel } from '../objects/disc-carousel';

@Component({
  selector: 'app-tempo-delete-me',
  templateUrl: './tempo-delete-me.component.html',
  styleUrls: ['./tempo-delete-me.component.scss']
})
export class TempoDeleteMeComponent implements OnInit {

  constructor(private client: GameClientService) { }

  selectedCarousel: DiscCarousel;
  discCarousels: DiscCarousel[] = [];
  slot = 1;

  ngOnInit(): void {
    console.log('test1');
    this.client.getDiscCarousels().subscribe(
      result => {
        console.log('test2');
        console.log(result);
        this.discCarousels = result;
      }
    );
  }

  ejectDisk(): void {
    console.log(this.selectedCarousel);
    if (this.selectedCarousel) {
      this.client.ejectDisc(this.selectedCarousel.uniqueID, this.slot).subscribe( o => {
        console.log(o);
      });
    }
  }
}
