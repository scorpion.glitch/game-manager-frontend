import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { GiantbombGame } from '../objects/giantbomb-game';
import { GameScraperDetailsComponent } from '../game-scraper-details/game-scraper-details.component';
import { GameManagerService } from '../services/game-manager.service';

@Component({
  selector: 'app-game-scraper-results',
  templateUrl: './game-scraper-results.component.html',
  styleUrls: ['./game-scraper-results.component.scss']
})
export class GameScraperResultsComponent implements OnInit {
  private _QUERY = '';
  games: GiantbombGame[];
  selectedGame: GiantbombGame;

  @ViewChild('gameScraperdetailsModal')
  gameScraperdetailsModal: GameScraperDetailsComponent;

  @Input()
  set query(query: string) {
    this._QUERY = query;
    this.update();
  }

  @Output() queryChange = new EventEmitter<string>();

  get query(): string {
    return this._QUERY;
  }

  constructor(private manager: GameManagerService) { }

  ngOnInit(): void {
    this.update();
  }

  update(): void {
    if (this.query.length === 0) {
      return;
    }
    this.manager.search(this.query).subscribe(
      response => {
        this.games = response;
      }
    );
  }

  setSelectedGame(game: GiantbombGame): void {
    this.selectedGame = game;
    this.gameScraperdetailsModal.show();
  }
}
