import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameScraperResultsComponent } from './game-scraper-results.component';

describe('GameScraperResultsComponent', () => {
  let component: GameScraperResultsComponent;
  let fixture: ComponentFixture<GameScraperResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameScraperResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameScraperResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
