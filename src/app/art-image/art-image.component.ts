import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-art-image',
  templateUrl: './art-image.component.html',
  styleUrls: ['./art-image.component.scss']
})
export class ArtImageComponent implements OnInit {
  @Input() imageSrc = '/assets/images/not_found.png';
  @Input() imageWidth?: number;
  @Input() imageHeight?: number;

  constructor() { }

  ngOnInit(): void {
    console.log(this.imageSrc);
  }

}
