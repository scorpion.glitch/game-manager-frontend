import { Injectable } from '@angular/core';
import { GameManagerGame } from '../objects/game-manager-game';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GameManagerPlatform } from '../objects/game-manager-platform';
import { GiantbombGame } from '../objects/giantbomb-game';
import { GameManagerPage } from '../objects/game-manager-page';

@Injectable({
  providedIn: 'root'
})
export class GameManagerService {
  private gameManagerUrl = 'http://127.0.0.1:9015/api/gamemanager/';

  constructor(private http: HttpClient) { }

  importGame(releaseGUID: string, artImageURL: string): Observable<GameManagerGame> {
    return this.http.post<GameManagerGame>(this.gameManagerUrl + 'import/' + releaseGUID, artImageURL);
  }

  search(query: string): Observable<GiantbombGame[]> {
    return this.http.get<GiantbombGame[]>(this.gameManagerUrl + 'searchgame/' + query);
  }

  addGame(gameManagerGame: GameManagerGame): Observable<GameManagerGame> {
    return this.http.post<GameManagerGame>(this.gameManagerUrl + 'game', gameManagerGame);
  }

  getGames(): Observable<GameManagerPage> {
    return this.http.get<GameManagerPage>(this.gameManagerUrl + 'games');
  }

  getGame(gameId: string): Observable<GameManagerGame> {
    return this.http.get<GameManagerGame>(this.gameManagerUrl + 'game/id/' + gameId);
  }

  addPlatform(gameManagerPlatform: GameManagerPlatform): Observable<GameManagerPlatform> {
    return this.http.post<GameManagerPlatform>(this.gameManagerUrl + 'platform', gameManagerPlatform);
  }

  getPlatform(platformId: number): Observable<GameManagerPlatform> {
    return this.http.get<GameManagerPlatform>(this.gameManagerUrl + 'platform/' + platformId);
  }

  getPlatformByName(name: string): Observable<GameManagerPlatform> {
    return this.http.get<GameManagerPlatform>(this.gameManagerUrl + 'platform/name' + name);
  }
}
