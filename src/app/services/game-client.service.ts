import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DiscCarousel } from '../objects/disc-carousel';

@Injectable({
  providedIn: 'root'
})
export class GameClientService {
  private gameClientURL = 'http://127.0.0.1:1186/api';

  constructor(private http: HttpClient) { }

  getDiscCarousels(): Observable<DiscCarousel[]> {
    return this.http.get<DiscCarousel[]>(this.gameClientURL + '/carousel');
  }

  ejectDisc(carouselID: string, slot: number): Observable<object> {
    return this.http.get<object>(this.gameClientURL + '/carousel/' + carouselID + '/eject/' + slot);
  }
}
