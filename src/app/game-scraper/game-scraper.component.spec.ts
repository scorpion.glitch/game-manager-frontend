import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameScraperComponent } from './game-scraper.component';

describe('GameScraperComponent', () => {
  let component: GameScraperComponent;
  let fixture: ComponentFixture<GameScraperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameScraperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameScraperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
