import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-scraper',
  templateUrl: './game-scraper.component.html',
  styleUrls: ['./game-scraper.component.scss']
})
export class GameScraperComponent implements OnInit {
  searchString: string;
  query = '';

  constructor() { }

  ngOnInit(): void {
  }

  search(): void {
    this.query = this.searchString;
  }

  keyDown(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.search();
    }
  }
}
