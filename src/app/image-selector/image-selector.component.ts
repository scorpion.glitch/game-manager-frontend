import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-image-selector',
  templateUrl: './image-selector.component.html',
  styleUrls: ['./image-selector.component.scss']
})
export class ImageSelectorComponent implements OnInit {
  @Input()
  image: string;

  @Output()
  imageChange = new EventEmitter<string>();

  @Input()
  images: string[];

  selecting = false;

  constructor() { }

  ngOnInit(): void {
  }

  selectImage(image: string): void {
    this.image = image;
    this.imageChange.emit(this.image);
  }

  showSelector(): void {
    this.selecting = true;
  }
  hideSelector(): void {
    this.selecting = false;
  }
}
