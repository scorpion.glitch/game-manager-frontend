import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GiantbombGame } from '../objects/giantbomb-game';
import { GiantbombRelease } from '../objects/giantbomb-release';
import { GameManagerGame } from '../objects/game-manager-game';
import { GameManagerService } from '../services/game-manager.service';

@Component({
  selector: 'app-game-scraper-details',
  templateUrl: './game-scraper-details.component.html',
  styleUrls: ['./game-scraper-details.component.scss']
})
export class GameScraperDetailsComponent implements OnInit {

  @ViewChild('gameScraperDetailsContainer')
  gameScraperDetailsContainer: any;

  private _GAME: GiantbombGame;
  gameManagerGame?: GameManagerGame;
  _RELEASE?: GiantbombRelease | undefined;
  images: string[];
  visible = false;

  constructor(private manager: GameManagerService) { }

  ngOnInit(): void {}

  get release(): GiantbombRelease | undefined {
    return this._RELEASE;
  }

  set release(release: GiantbombRelease | undefined) {
    this._RELEASE = release;
  }

  get game(): GiantbombGame {
    return this._GAME;
  }

  @Input()
  set game(game: GiantbombGame) {
    this.images = [];
    delete this._RELEASE;
    delete this.gameManagerGame;
    this._GAME = game;
  }

  show(): void {
    this.visible = true;
  }

  hide(): void {
    this.visible = false;
    this.images = [];
    delete this._RELEASE;
    delete this.gameManagerGame;
  }

  addGame(): void {
    if (this.gameManagerGame) {
      this.manager.addGame(this.gameManagerGame).subscribe(
        g => {
          console.log(g);
        }
      );
    }
    this.hide();
  }
}
