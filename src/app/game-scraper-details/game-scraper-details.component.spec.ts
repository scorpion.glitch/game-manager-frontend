import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameScraperDetailsComponent } from './game-scraper-details.component';

describe('GameScraperDetailsComponent', () => {
  let component: GameScraperDetailsComponent;
  let fixture: ComponentFixture<GameScraperDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameScraperDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameScraperDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
