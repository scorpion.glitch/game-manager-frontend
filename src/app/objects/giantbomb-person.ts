import { GiantbombCharacter } from './giantbomb-character';
import { GiantbombConcept } from './giantbomb-concept';
import { GiantbombGame } from './giantbomb-game';
import { GiantbombFranchise } from './giantbomb-franchise';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombImageTag } from './giantbomb-image-tag';
import { GiantbombLocation } from './giantbomb-location';
import { GiantbombObject } from './giantbomb-object';
import { GiantbombSearch } from './giantbomb-search';

export interface GiantbombPerson extends GiantbombSearch {
    aliases: string;
    api_detail_url: string;
    birth_date: string;
    characters: GiantbombCharacter[];
    concepts: GiantbombConcept[];
    country: string;
    date_added: string;
    date_last_updated: string;
    death_date: string;
    deck: string;
    description: string;
    first_credited_game: GiantbombGame;
    franchises: GiantbombFranchise[];
    games: GiantbombGame[];
    gender: number;
    guid: string;
    hometown: string;
    id: number;
    image: GiantbombImage;
    image_tags: GiantbombImageTag[];
    locations: GiantbombLocation[];
    name: string;
    objects: GiantbombObject[];
    people: GiantbombPerson[];
    site_detail_url: string;
}
