export interface GiantbombVideoCatagory {
    api_detail_url: string;
    id: number;
    name: string;
    site_detail_url: string;
}
