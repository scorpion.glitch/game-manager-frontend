import { GameManagerSort } from './game-manager-sort';

export interface GameManagerPageable {
    sort: GameManagerSort;
    offset: number;
    pageSize: number;
    pageNumber: number;
    paged: boolean;
    unpaged: boolean;
}
