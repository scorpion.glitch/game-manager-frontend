import { GameManagerCompany } from './game-manager-company';

export interface GameManagerPlatform {
    id: number;
    abbreviation: string;
    company: GameManagerCompany;
    deck: string;
    description: string;
    image: string;
    name: string;
    originalPrice: string;
    releaseDate: string;
    siteDetailUrl: string;
}
