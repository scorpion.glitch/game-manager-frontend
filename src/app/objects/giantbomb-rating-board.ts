import { GiantbombImage } from './giantbomb-image';
import { GiantbombRegion } from './giantbomb-region';

export interface GiantbombRatingBoard {
    api_detail_url: string;
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    guid: string;
    id: number;
    image: GiantbombImage;
    name: string;
    region: GiantbombRegion;
}
