import { GiantbombCharacter } from './giantbomb-character';
import { GiantbombConcept } from './giantbomb-concept';
import { GiantbombGame } from './giantbomb-game';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombImageTag } from './giantbomb-image-tag';
import { GiantbombLocation } from './giantbomb-location';
import { GiantbombObject } from './giantbomb-object';
import { GiantbombPerson } from './giantbomb-person';
import { GiantbombSearch } from './giantbomb-search';

export interface GiantbombFranchise extends GiantbombSearch {
    aliases: string;
    api_detail_url: string;
    characters: GiantbombCharacter[];
    concepts: GiantbombConcept[];
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    games: GiantbombGame[];
    guid: string;
    id: number;
    image: GiantbombImage;
    image_tags: GiantbombImageTag[];
    locations: GiantbombLocation[];
    name: string;
    objects: GiantbombObject[];
    people: GiantbombPerson[];
    site_detail_url: string;
}
