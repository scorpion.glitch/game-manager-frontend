export interface GameManagerCompany {
    id: number;
    abbreviation: string;
    aliases: string;
    dateFounded: string;
    deck: string;
    description: string;
    image: string;
    locationAddress: string;
    locationCity: string;
    locationCountry: string;
    locationState: string;
    name: string;
    phone: string;
    siteDetailURL: string;
    website: string;
}
