import { GiantbombCharacter } from './giantbomb-character';
import { GiantbombConcept } from './giantbomb-concept';
import { GiantbombGame } from './giantbomb-game';
import { GiantbombRelease } from './giantbomb-release';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombImageTag } from './giantbomb-image-tag';
import { GiantbombLocation } from './giantbomb-location';
import { GiantbombObject } from './giantbomb-object';
import { GiantbombPerson } from './giantbomb-person';
import { GiantbombSearch } from './giantbomb-search';

export interface GiantbombCompany extends GiantbombSearch {
    abbreviation: string;
    aliases: string;
    api_detail_url: string;
    characters: GiantbombCharacter[];
    concepts: GiantbombConcept[];
    date_added: string;
    date_founded: string;
    date_last_updated: string;
    deck: string;
    description: string;
    developed_games: GiantbombGame[];
    developer_releases: GiantbombRelease[];
    distributor_releases: GiantbombRelease[];
    guid: string;
    id: number;
    image: GiantbombImage;
    image_tags: GiantbombImageTag[];
    location_address: string;
    location_city: string;
    location_country: string;
    location_state: string;
    locations: GiantbombLocation;
    name: string;
    objects: GiantbombObject[];
    people: GiantbombPerson[];
    phone: string;
    published_games: GiantbombGame[];
    publisher_releases: GiantbombRelease[];
    site_detail_url: string;
    website: string;
}
