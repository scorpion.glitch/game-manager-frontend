export interface GiantbombSoundSystem {
    api_detail_url: string;
    id: number;
    name: string;
}
