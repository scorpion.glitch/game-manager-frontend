export interface DiscCarousel {
    uniqueID: string;
    connected: boolean;
    capacity: number;
}
