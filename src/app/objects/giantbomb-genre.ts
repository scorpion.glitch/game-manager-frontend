import { GiantbombImage } from './giantbomb-image';

export interface GiantbombGenre {
    api_detail_url: string;
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    guid: string;
    id: number;
    image: GiantbombImage;
    name: string;
    site_detail_url: string;
}
