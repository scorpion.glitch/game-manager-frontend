import { GiantbombDLC } from './giantbomb-dlc';
import { GiantbombGame } from './giantbomb-game';
import { GiantbombPlatform } from './giantbomb-platform';
import { GiantbombRelease } from './giantbomb-release';

export interface GiantbombReview {
    api_detail_url: string;
    deck: string;
    description: string;
    dlc: GiantbombDLC;
    dlc_name: string;
    game: GiantbombGame;
    guid: string;
    id: number;
    name: string;
    platforms: GiantbombPlatform[];
    publish_date: string;
    release: GiantbombRelease;
    reviewer: string;
    score: number;
    site_detail_url: string;
}
