import { GiantbombGame } from './giantbomb-game';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombImageTag } from './giantbomb-image-tag';
import { GiantbombSearch } from './giantbomb-search';

export interface GiantbombLocation extends GiantbombSearch {
    aliases: string;
    api_detail_url: string;
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    first_appeared_in_game: GiantbombGame;
    guid: string;
    id: number;
    image: GiantbombImage;
    image_tags: GiantbombImageTag[];
    name: string;
    site_detail_url: string;
}
