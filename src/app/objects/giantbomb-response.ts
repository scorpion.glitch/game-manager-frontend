export interface GiantbombResponse<T> {
  status_code: number;
  error: string;
  number_of_total_results: number;
  number_of_page_results: number;
  limit: number;
  offset: number;
  results: T;
  version: string;
}
