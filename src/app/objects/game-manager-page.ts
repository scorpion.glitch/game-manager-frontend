import { GameManagerGame } from './game-manager-game';
import { GameManagerSort } from './game-manager-sort';
import { GameManagerPageable } from './game-manager-pageable';

export interface GameManagerPage {
    content: GameManagerGame[];
    pageable: GameManagerPageable;
    totalElements: number;
    last: boolean;
    totalPages: number;
    size: number;
    number: number;
    sort: GameManagerSort;
    numberOfELements: number;
    first: boolean;
    empty: boolean;
}
