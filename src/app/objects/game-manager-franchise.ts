export interface GameManagerFranchise {
    id: number;
    aliases: string;
    siteDetailUrl: string;
    deck: string;
    description: string;
    image: string;
    name: string;
}
