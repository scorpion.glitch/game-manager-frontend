import { GiantbombImage } from './giantbomb-image';
import { GiantbombVideo } from './giantbomb-video';

export interface GiantbombVideoShow {
    api_detail_url: string;
    deck: string;
    guid: string;
    id: number;
    title: string;
    position: number;
    image: GiantbombImage;
    logo: GiantbombImage;
    site_detail_url: string;
    active: boolean;
    display_nav: boolean;
    latest: GiantbombVideo;
    premium: boolean;
    api_videos_url: string;
}
