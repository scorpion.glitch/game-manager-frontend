export interface GameManagerGenre {
    id: number;
    deck: string;
    description: string;
    image: string;
    name: string;
}
