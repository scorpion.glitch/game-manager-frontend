export interface GameManagerSort {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
}
