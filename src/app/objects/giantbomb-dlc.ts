import { GiantbombGame } from './giantbomb-game';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombPlatform } from './giantbomb-platform';

export interface GiantbombDLC {
    api_detail_url: string;
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    game: GiantbombGame;
    guid: string;
    id: number;
    image: GiantbombImage;
    name: string;
    platform: GiantbombPlatform;
    release_date: Date;
    site_detail_url: string;
}
