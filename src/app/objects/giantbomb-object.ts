import { GiantbombCharacter } from './giantbomb-character';
import { GiantbombCompany } from './giantbomb-company';
import { GiantbombConcept } from './giantbomb-concept';
import { GiantbombGame } from './giantbomb-game';
import { GiantbombFranchise } from './giantbomb-franchise';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombImageTag } from './giantbomb-image-tag';
import { GiantbombLocation } from './giantbomb-location';
import { GiantbombPerson } from './giantbomb-person';
import { GiantbombSearch } from './giantbomb-search';

export interface GiantbombObject extends GiantbombSearch {
    aliases: string;
    api_detail_url: string;
    characters: GiantbombCharacter[];
    companies: GiantbombCompany[];
    concepts: GiantbombConcept[];
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    first_appeared_in_game: GiantbombGame;
    franchises: GiantbombFranchise[];
    games: GiantbombGame[];
    guid: string;
    id: number;
    image: GiantbombImage;
    image_tags: GiantbombImageTag[];
    locations: GiantbombLocation[];
    name: string;
    objects: GiantbombObject[];
    people: GiantbombPerson[];
    site_detail_url: string;
}
