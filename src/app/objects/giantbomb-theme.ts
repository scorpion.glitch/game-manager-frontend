export interface GiantbombTheme {
    api_detail_url: string;
    guid: string;
    id: number;
    name: string;
    site_detail_url: string;
}
