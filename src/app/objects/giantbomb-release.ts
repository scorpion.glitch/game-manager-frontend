import { GiantbombCompany } from './giantbomb-company';
import { GiantbombGame } from './giantbomb-game';
import { GiantbombGameRating } from './giantbomb-game-rating';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombMultiplayerFeature } from './giantbomb-multiplayer-feature';
import { GiantbombPlatform } from './giantbomb-platform';
import { GiantbombRegion } from './giantbomb-region';
import { GiantbombResolution } from './giantbomb-resolution';
import { GiantbombSingleplayerFeature } from './giantbomb-singleplayer-feature';
import { GiantbombSoundSystem } from './giantbomb-sound-system';

export interface GiantbombRelease {
    api_detail_url: string;
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    developers: GiantbombCompany[];
    expected_release_day: number;
    expected_release_month: number;
    expected_release_quarter: number;
    expected_release_year: number;
    game: GiantbombGame;
    game_rating: GiantbombGameRating;
    guid: string;
    id: number;
    image: GiantbombImage;
    images: GiantbombImage[];
    maximum_players: number;
    minimum_players: number;
    multiPlayerFeatures: GiantbombMultiplayerFeature[];
    name: string;
    platform: GiantbombPlatform;
    product_code_type: string;
    product_code_value: string;
    publishers: GiantbombCompany[];
    region: GiantbombRegion;
    release_date: string;
    resolutions: GiantbombResolution[];
    singlePlayerFeatures: GiantbombSingleplayerFeature[];
    sound_systems: GiantbombSoundSystem[];
    site_detail_url: string;
    widescreen_support: number;
}
