import { GiantbombObject } from './giantbomb-object';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombVideoCatagory } from './giantbomb-video-catagory';
import { GiantbombVideoShow } from './giantbomb-video-show';
import { GiantbombSearch } from './giantbomb-search';

export interface GiantbombVideo extends GiantbombSearch {
    api_detail_url: string;
    associations: GiantbombObject[];
    deck: string;
    hd_url: string;
    high_url: string;
    low_url: string;
    embed_player: string;
    guid: string;
    id: number;
    image: GiantbombImage;
    length_seconds: number;
    name: string;
    publish_date: string;
    site_detail_url: string;
    url: string;
    user: string;
    video_categories: GiantbombVideoCatagory[];
    video_type: string;
    video_show: GiantbombVideoShow;
    youtube_id: string;
    saved_time: number;
    premium: boolean;
    hosts: string;
    crew: string;
}
