export interface GiantbombImage {
    icon_url: string;
    medium_url: string;
    original_url: string;
    screen_url: string;
    screen_large_url: string;
    small_url: string;
    super_url: string;
    thumb_url: string;
    tiny_url: string;
    image_tags: string;
    original: string;
    tags: string;
}
