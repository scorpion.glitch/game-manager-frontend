import { GiantbombCompany } from './giantbomb-company';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombImageTag } from './giantbomb-image-tag';

export interface GiantbombPlatform {
    abbreviation: string;
    api_detail_url: string;
    company: GiantbombCompany;
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    guid: string;
    id: number;
    image: GiantbombImage;
    image_tags: GiantbombImageTag[];
    install_base: string;
    name: string;
    online_support: boolean;
    original_price: string;
    release_date: string;
    site_detail_url: string;
}
