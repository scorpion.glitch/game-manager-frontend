import { GiantbombImage } from './giantbomb-image';
import { GiantbombRatingBoard } from './giantbomb-rating-board';

export interface GiantbombRegion {
    api_detail_url: string;
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    guid: string;
    id: number;
    image: GiantbombImage;
    name: string;
    rating_boards: GiantbombRatingBoard[];
    site_detail_url: string;
}
