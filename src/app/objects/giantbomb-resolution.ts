export interface GiantbombResolution {
    api_detail_url: string;
    id: number;
    name: string;
}
