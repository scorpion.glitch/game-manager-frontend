import { GiantbombGame } from './giantbomb-game';
import { GiantbombImage } from './giantbomb-image';
import { GiantbombImageTag } from './giantbomb-image-tag';
import { GiantbombSearch } from './giantbomb-search';

export interface GiantbombCharacter extends GiantbombSearch {
    aliases: string;
    api_detail_url: string;
    birthday: string;
    date_added: string;
    date_last_updated: string;
    deck: string;
    description: string;
    first_appeared_in_game: GiantbombGame;
    gender: number;
    guid: string;
    id: number;
    image: GiantbombImage;
    image_tags: GiantbombImageTag[];
    last_name: string;
    name: string;
    real_name: string;
    site_detail_url: string;
}
