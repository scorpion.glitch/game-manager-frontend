export interface GiantbombImageTag {
    api_detail_url: string;
    name: string;
    total: number;
}
