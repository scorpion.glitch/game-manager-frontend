import { GameManagerCompany } from './game-manager-company';
import { GameManagerFranchise } from './game-manager-franchise';
import { GameManagerGenre } from './game-manager-genre';
import { GameManagerPlatform } from './game-manager-platform';

export interface GameManagerGame {
    id: number;
    aliases: string;
    deck: string;
    description: string;
    developers: GameManagerCompany[];
    franchises: GameManagerFranchise[];
    game_rating: string;
    genres: GameManagerGenre[];
    cover_image: string;
    art_image: string;
    name: string;
    platform: GameManagerPlatform;
    publishers: GameManagerCompany[];
    region: string;
    release_date: string;
    site_detail_url_game: string;
    site_detail_url_release: string;
}
