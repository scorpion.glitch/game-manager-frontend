import { GiantbombImage } from './giantbomb-image';
import { GiantbombRatingBoard } from './giantbomb-rating-board';

export interface GiantbombGameRating {
    api_detail_url: string;
    guid: string;
    id: number;
    image: GiantbombImage;
    name: string;
    rating_board: GiantbombRatingBoard;
}
