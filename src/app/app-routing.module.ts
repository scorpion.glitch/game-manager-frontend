import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameScraperComponent } from './game-scraper/game-scraper.component';
import { GamesLibraryComponent } from './games-library/games-library.component';

const routes: Routes = [
  { path: 'addgame', component: GameScraperComponent },
  { path: 'games', component: GamesLibraryComponent },
  { path: '', redirectTo: '/games', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
