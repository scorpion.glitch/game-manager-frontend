import { Component, OnInit, Input, ElementRef, Self } from '@angular/core';
import { GameManagerGame } from '../objects/game-manager-game';

@Component({
  selector: 'app-games-library-item',
  templateUrl: './games-library-item.component.html',
  styleUrls: ['./games-library-item.component.scss']
})
export class GamesLibraryItemComponent implements OnInit {
  @Input() game: GameManagerGame;

  ne: any;
  el: ElementRef;
  yRotation = 0;
  xRotation = 0;
  scale = 1.1;

  constructor(private ref: ElementRef) {
    this.el = ref;
    /*
    this.el.nativeElement.addEventListener('mousemove', this.handleMove);
    this.el.nativeElement.addEventListener('mouseout', this.handleOut);
    this.el.nativeElement.addEventListener('mousedown', this.handleDown);
    this.el.nativeElement.addEventListener('mouseup', this.handleUp);
    */
    this.ne = ref.nativeElement;
  }

  ngOnInit(): void {
  }

  setRotation(x: number, y: number) {
    this.xRotation = x;
    this.yRotation = y;
  }

  handleMove(e: any): void {
    const xVal = e.layerX;
    const yVal = e.layerY;
    const temp = e.explicitOriginalTarget.parentNode.parentNode.parentNode.parentNode;
    const width = temp.offsetWidth;
    const height = temp.offsetHeight;

    this.yRotation = 20 * ((xVal - width / 2) / width);
    this.xRotation = -20 * ((yVal - height / 2) / height);

    this.scale = 1.1;
  }

  handleOut(e: any): void {
    this.yRotation = 0;
    this.xRotation = 0;
    this.scale = 1.0;
  }

  handleDown(e: any): void {
    this.yRotation = 0;
    this.xRotation = 0;
    this.scale = 1.0;
  }

  handleUp(e: any): void {
    this.yRotation = 0;
    this.xRotation = 0;
    this.scale = 1.0;
  }

  getStyle(): any {
    const transformation = 'perspective(500px) scale(' + this.scale + ') rotateX(' + this.xRotation + ') rotateY(' + this.yRotation + ')';
    console.log(transformation)
    return {
      'height.px': '180',
      'width.px': '120',
      transform: transformation
    };
  }
}
