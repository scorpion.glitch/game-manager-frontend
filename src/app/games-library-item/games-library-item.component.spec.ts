import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesLibraryItemComponent } from './games-library-item.component';

describe('GamesLibraryItemComponent', () => {
  let component: GamesLibraryItemComponent;
  let fixture: ComponentFixture<GamesLibraryItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamesLibraryItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesLibraryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
